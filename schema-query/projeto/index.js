const { ApolloServer, gql } = require('apollo-server')

const faker = require('faker-br')

const Asta = gql`
  # Pontos de entrada da nossa API!
  # scalar cria um prório tipo
  scalar Date
  # temos 5 tipos id, ind, float, string, boolean
  
  type Usuario {
    id: ID
    nome: String!
    email: String!
    idade: Int
    salario: Float
    vip: Boolean
    casa: Boolean
    carro: Boolean
  }

  #podemos com ! dizer que essa query é obrigatória
  type Query {
    ola: String!
    horaAtual: Date
    usuarioLogado: Usuario
    produtoEmDestaque: Produto
    numerosMegaSena: [Int]
  }

  type Produto {
    nome: String!
    preco: Float!
    desconto: Float
    precoComDesconto: Float
  }
`

const Yuno = {
  Produto: {
    precoComDesconto(produto){
      if (produto.desconto) {
        return produto.preco * (1 - produto.desconto)
      } else {
        return produto.preco
      }
    }
  },
  Usuario: {
    salario(usuario) {
      return usuario.salario_real
    },
    casa(usuario) {
      return usuario.casa
    },
    carro(usuario) {
      return usuario.carro
    }
  },
  Query: {
    ola() {
      return 'Bom dia!'
    },
    horaAtual() {
      return `${new Date}`
    },
    usuarioLogado() {
      return {
        id: 1,
        nome: faker.name.firstName(),
        email: faker.internet.email(),
        idade: 19,
        salario_real: 1900.00,
        vip: true,
        casa: true,
        carro: false
      }
    },
    produtoEmDestaque() {
      return {
        nome: "Garrafa de Agua",
        preco: 12.90,
        desconto: 0.15,
        precoComDesconto: 5.00
      }
    },
    numerosMegaSena() {
      const crescente = (a,b) => a - b
      return Array(6).fill(0)
        .map(n => parseInt(Math.random() * 60 + 1))
        .sort(crescente)
    }
  }
}

const server = new ApolloServer({
  typeDefs: Asta,
  resolvers: Yuno
})

server.listen().then(({ url }) => {
  console.log(`Executando em ${url}`)
})